import os

# You need to replace the next values with the appropriate values for your configuration
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
db = os.environ['BLESSCARD_DB']
user = os.environ['BLESSCARD_USER']
host = os.environ['BLESSCARD_HOST']
pssw = os.environ['BLESSCARD_PASS']
SQLALCHEMY_DATABASE_URI = "postgresql://%s:%s@%s/%s" % (user,pssw,host,db)
#print "SQLALCHEMY_DATABASE_URI %s" % SQLALCHEMY_DATABASE_URI