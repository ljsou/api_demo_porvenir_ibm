# API Template

## Use

* Flask
* Flask-SQLAlchemy (ORM for database)

```bash
pip install -r requirements.txt
python migrate.py db init (Execute this line just if not exists the Migration folder)
python migrate.py db migrate
python migrate.py db upgrade
```

## Run

```bash
python run.py
```

