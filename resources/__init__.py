from .customer.get_customer import GetCustomerResource
from .customer.verify import VerifyResource
from .device.query_device import QueryDeviceResource