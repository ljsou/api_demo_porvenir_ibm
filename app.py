from flask import Blueprint
from flask_restful import Api

from resources.customer import GetCustomerResource
from resources.customer import VerifyResource
from resources.device import QueryDeviceResource




api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route Customers
api.add_resource(GetCustomerResource, '/customers/<int:identification_number>')
api.add_resource(VerifyResource, '/customers/verify')

# Route Device
api.add_resource(QueryDeviceResource, '/query_device')




