from flask import Flask
import os
import rollbar
import rollbar.contrib.flask
from flask import got_request_exception
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask_jwt_extended import JWTManager
import datetime

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    app.config['DEFAULT_PARSERS'] = [
        'flask.ext.api.parsers.JSONParser',
        'flask.ext.api.parsers.URLEncodedParser',
        'flask.ext.api.parsers.MultiPartParser'
    ]

    # Configuracion para JWT Authorization
    app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(minutes=5)
    jwt = JWTManager(app)
    
    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/porvenir/api/1.0')

    from Model import db
    db.init_app(app)

    # import rollbar

    # rollbar.init(
    #     # access token for the demo app: https://rollbar.com/demo
    #     '9ffbb3d9930c46018c0832e17683fb2a',
    #     # environment name
    #     'development',
    #     # server root directory, makes tracebacks prettier
    #     root=os.path.dirname(os.path.realpath(__file__)),
    #     # flask already sets up logging
    #     allow_logging_basic_config=False)

    # # send exceptions from `app` to rollbar, using flask's signal system.
    # got_request_exception.connect(rollbar.contrib.flask.report_exception, app)
    # rollbar.report_message('Rollbar in Botmmerce-BlessCard is configured correctly')

    return app, db

app, db = create_app("config")
#migrate = Migrate(app, db)
#manager = Manager(app)
#manager.add_command('db', MigrateCommand)

if __name__ == "__main__":
   #app = create_app("config")
   app.run(port=5000, ssl_context='adhoc', debug=True)
   #app.run(host=host, port=5000, debug=True)


# if __name__ == '__main__':
#     app.run()

