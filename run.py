from flask import Flask
import os
import rollbar
import rollbar.contrib.flask
from flask import got_request_exception
from flask_jwt_extended import JWTManager
import datetime

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    
    # Configuracion para JWT Authorization
    app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)
    jwt = JWTManager(app)

    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/porvenir/api/1.0')

    import rollbar

    rollbar.init(
        # access token for the demo app: https://rollbar.com/demo
        '235ba172c50f498197a542101c950c48',
        # environment name
        'development',
        # server root directory, makes tracebacks prettier
        root=os.path.dirname(os.path.realpath(__file__)),
        # flask already sets up logging
        allow_logging_basic_config=False)

    # send exceptions from `app` to rollbar, using flask's signal system.
    got_request_exception.connect(rollbar.contrib.flask.report_exception, app)
    rollbar.report_message('Rollbar in DEMO_Porvnir_IBM is configured correctly')

    return app

app= create_app("config")

if __name__ == "__main__":
    app.run(ssl_context='adhoc', debug=True)


